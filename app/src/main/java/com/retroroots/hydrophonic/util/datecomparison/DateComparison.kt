package com.retroroots.hydrophonic.util.datecomparison

import java.util.*

interface DateComparison
{
    fun greaterThan(lesserDate : Date, greaterDate : Date, calendarField : Int, calendarFieldAddition : Int) : Boolean
}