package com.retroroots.hydrophonic.util.datecomparison

import java.util.*

//todo create a test for this.
//todo implement this on the ph fragment.
object DateComparisonHelper : DateComparison
{
    /**
     * Compare if lesserDate is lesser than greaterDate.
     *
     * @param calendarField Which part of the calendar is being altered. Ex, Calendar.MINUTE
     *
     * @param calendarFieldAddition By how much is the calendarField being altered. Ex, add one minute. Then, calendarField = Calendar.MINUTE and calendarFieldAddition = 1
     */
    override fun greaterThan(lesserDate: Date, greaterDate: Date, calendarField: Int, calendarFieldAddition: Int): Boolean
    {
        if(calendarFieldAddition == 0)
            return false

        val greaterCalendar = Calendar.getInstance()

        greaterCalendar.time = greaterDate

        val lesserCalendar = Calendar.getInstance()

        lesserCalendar.time = lesserDate

        lesserCalendar.add(calendarField, calendarFieldAddition)

        return greaterCalendar >= lesserCalendar
    }

}