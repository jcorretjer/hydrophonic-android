package com.retroroots.hydrophonic.util.pistatuscolorfinder

interface PiStatusColorFinder
{
    enum class PiStatus
    {
        RUNNING,
        FAILED
    }

    fun find(status : PiStatus) : Int
}