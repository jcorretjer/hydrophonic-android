package com.retroroots.hydrophonic.util.phanalyzer.listeners

interface AlkalineEventListener
{
    fun onAdd()
}