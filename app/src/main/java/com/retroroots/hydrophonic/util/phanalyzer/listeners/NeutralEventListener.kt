package com.retroroots.hydrophonic.util.phanalyzer.listeners

interface NeutralEventListener
{
    fun onStable()
}