package com.retroroots.hydrophonic.util.phcolorfinder

interface PhColorFinder
{
    fun find(ph : Int) : Int
}