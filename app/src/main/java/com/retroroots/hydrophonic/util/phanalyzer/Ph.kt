package com.retroroots.hydrophonic.util.phanalyzer

data class Ph(
        val value : Double,

        val alkalineThreshold : Double,

        val acidThreshold : Double,

        val userNeutral : Double
)
