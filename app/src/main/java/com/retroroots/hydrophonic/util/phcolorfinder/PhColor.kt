package com.retroroots.hydrophonic.util.phcolorfinder

import com.retroroots.hydrophonic.R

object PhColor : PhColorFinder
{
    /**
     * @return The R color id. If it doesn't find it, defaults to the color id for a ph of 13
     */
    override fun find(ph: Int): Int =
        when(ph)
        {
            0 -> R.color.ph0

            1 -> R.color.ph1

            2 -> R.color.ph2

            3 -> R.color.ph3

            4 -> R.color.ph4

            5 -> R.color.ph5

            6 -> R.color.ph6

            7 -> R.color.ph7

            8 -> R.color.ph8

            9 -> R.color.ph9

            10 -> R.color.ph10

            11 -> R.color.ph11

            12 -> R.color.ph12

            else -> R.color.ph13
        }

}