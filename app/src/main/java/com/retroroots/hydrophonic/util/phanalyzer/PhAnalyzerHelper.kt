package com.retroroots.hydrophonic.util.phanalyzer

import com.retroroots.hydrophonic.util.phanalyzer.listeners.AcidEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AlkalineEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.NeutralEventListener

object PhAnalyzerHelper : PhAnalyzer
{
    private var acidEventListener: AcidEventListener? = null

    private var alkalineEventListener: AlkalineEventListener? = null

    private var neutralEventListener: NeutralEventListener? = null

    override fun addAcidEventListener(acidEventListener: AcidEventListener)
    {
        PhAnalyzerHelper.acidEventListener = acidEventListener
    }

    override fun addAlkalineEventListener(alkalineEventListener: AlkalineEventListener)
    {
        PhAnalyzerHelper.alkalineEventListener = alkalineEventListener
    }

    override fun addNeutralEventListener(neutralEventListener: NeutralEventListener)
    {
        PhAnalyzerHelper.neutralEventListener = neutralEventListener
    }

    /**
     * Executes all ph event listeners, based on the comparison between the ph value and the thresholds.
     */
    override fun analyze(ph: Ph)
    {
        when
        {
            ph.value <= ph.acidThreshold ->
            {
                alkalineEventListener?.onAdd()
            }

            ph.value >= ph.alkalineThreshold ->
            {
                acidEventListener?.onAdd()
            }

            ph.value in ph.acidThreshold..ph.alkalineThreshold ->
            {
                neutralEventListener?.onStable()
            }
        }
    }
}