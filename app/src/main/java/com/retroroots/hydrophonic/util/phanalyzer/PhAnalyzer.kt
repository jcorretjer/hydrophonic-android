package com.retroroots.hydrophonic.util.phanalyzer

import com.retroroots.hydrophonic.util.phanalyzer.listeners.AcidEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AlkalineEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.NeutralEventListener

interface PhAnalyzer
{
    fun addAcidEventListener(acidEventListener: AcidEventListener)

    fun addAlkalineEventListener(alkalineEventListener: AlkalineEventListener)

    fun addNeutralEventListener(neutralEventListener: NeutralEventListener)

    fun analyze(ph: Ph)
}
