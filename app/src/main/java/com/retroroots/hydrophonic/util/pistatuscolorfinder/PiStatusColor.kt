package com.retroroots.hydrophonic.util.pistatuscolorfinder

import com.retroroots.hydrophonic.R

object PiStatusColor: PiStatusColorFinder
{
    override fun find(status: PiStatusColorFinder.PiStatus): Int =
        when(status)
        {
            PiStatusColorFinder.PiStatus.RUNNING -> R.color.ph9

            else -> R.color.ph2
        }
}