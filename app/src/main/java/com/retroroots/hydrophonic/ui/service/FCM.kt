package com.retroroots.hydrophonic.ui.service

import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.retroroots.hydrophonic.R
import com.retroroots.hydrophonic.ui.MainActivity

/*
    If the notifications aren't arriving when the app is closed, restart the phone, lunch the app, once it boots, and then close it. Now it should be ok.
 */

class FCM : FirebaseMessagingService()
{
    override fun onNewToken(p0: String)
    {
        super.onNewToken(p0)
    }

    override fun onMessageReceived(p0: RemoteMessage)
    {
        val notificationManagerCompat = NotificationManagerCompat.from(this)

        var notification : NotificationCompat.Builder

        p0.notification.apply {
            notification = NotificationCompat.Builder(baseContext, resources.getString(R.string.phNotificationChannelID))
                .setSmallIcon(R.drawable.ph_ntification_alert_ic)
                .setContentTitle(this?.title)
                .setContentText(this?.body)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(PendingIntent.getActivity(baseContext, 0, Intent(baseContext, MainActivity::class.java), 0))
        }

        notificationManagerCompat.notify(1, notification.build())
    }
}