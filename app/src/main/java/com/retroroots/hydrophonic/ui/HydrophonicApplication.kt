package com.retroroots.hydrophonic.ui

import android.app.Application

class HydrophonicApplication : Application()
{
    companion object FirebaseDBKey
    {
        const val PH = "ph"

        const val THRESHOLD = "threshold"

        const val USER_NEUTRAL = "userNeutral"

        const val LAST_UPDATED = "lastUpdated"
    }
}