package com.retroroots.hydrophonic.ui.ph

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.retroroots.hydrophonic.BuildConfig
import com.retroroots.hydrophonic.R
import com.retroroots.hydrophonic.ui.HydrophonicApplication
import com.retroroots.hydrophonic.util.datecomparison.DateComparisonHelper
import com.retroroots.hydrophonic.util.phanalyzer.Ph
import com.retroroots.hydrophonic.util.phanalyzer.PhAnalyzerHelper
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AcidEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AlkalineEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.NeutralEventListener
import com.retroroots.hydrophonic.util.phcolorfinder.PhColor
import com.retroroots.hydrophonic.util.pistatuscolorfinder.PiStatusColor
import com.retroroots.hydrophonic.util.pistatuscolorfinder.PiStatusColorFinder
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class PhFragment : Fragment(R.layout.fragment_ph)
{
    private lateinit var phTxtVw : TextView

    private lateinit var statusDescIconImgVw : ImageView

    private lateinit var statusDescTxtVw : TextView

    private lateinit var phTitleTxtVw : TextView

    private lateinit var phStatusIconImgVw : ImageView

    private lateinit var circularProcessVwStub : ViewStub

    private lateinit var valueEventListener: ValueEventListener

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        //region Init views

        phTxtVw = view.findViewById(R.id.phTxtVw)

        statusDescIconImgVw = view.findViewById(R.id.statusDescIconImgVw)

        statusDescTxtVw = view.findViewById(R.id.statusDescTxtVw)

        phTitleTxtVw = view.findViewById(R.id.phTitleTxtVw)

        phStatusIconImgVw = view.findViewById(R.id.phStatusIconImgVw)

        circularProcessVwStub = view.findViewById(R.id.circularProgressVwStub)

        circularProcessVwStub.inflate()

        circularProcessVwStub.visibility = View.VISIBLE

        //endregion

        Firebase.messaging.subscribeToTopic(BuildConfig.FIREBASE_SUBSCRIPTION_TOPIC).addOnCompleteListener(requireActivity())
        {
            if(!it.isSuccessful)
            {
                Toast.makeText(requireContext(), "Failed to subscribe this device", Toast.LENGTH_LONG).show()

                return@addOnCompleteListener
            }
        }

        //Sign and update UI
        Firebase.auth.signInWithEmailAndPassword(BuildConfig.FIREBASE_USER, BuildConfig.FIREBASE_PASSWD).addOnCompleteListener(requireActivity())
        {
            if(!it.isSuccessful)
            {
                Toast.makeText(requireContext(), "Failed to login", Toast.LENGTH_LONG).show()

                return@addOnCompleteListener
            }

            valueEventListener = object: ValueEventListener
            {
                override fun onDataChange(snapshot: DataSnapshot)
                {
                    circularProcessVwStub.visibility = View.GONE

                    val data = snapshot.value!! as HashMap<*, *>

                    //region Pi status

                    val piStatusCalendarAddition = 30

                    var lastUpdated : Date? = null

                    try
                    {
                        lastUpdated = SimpleDateFormat("MM/dd/yyyy, hh:mm:ss", Locale.getDefault()).parse(data[HydrophonicApplication.LAST_UPDATED] as String) !!
                    }

                    catch (e : ParseException)
                    {
                    }

                    view.findViewById<TextView>(R.id.piStatusTxtVw).apply {
                        if(lastUpdated != null && !DateComparisonHelper.greaterThan(lastUpdated, Date(), Calendar.MINUTE, piStatusCalendarAddition))
                            updatePiStatusUI(resources.getString(R.string.runningPhStatusTxt), ContextCompat.getColor(requireContext(), PiStatusColor.find(PiStatusColorFinder.PiStatus.RUNNING)), this)

                        else
                            updatePiStatusUI(resources.getString(R.string.failedPhStatusTxt), ContextCompat.getColor(requireContext(), PiStatusColor.find(PiStatusColorFinder.PiStatus.FAILED)), this)
                    }

                    //endregion

                    //region pH

                    val ph = data[HydrophonicApplication.PH] as Double

                    phTxtVw.apply {
                        text = DecimalFormat("00.00").format(ph)

                        setTextColor(ContextCompat.getColor(requireContext(), PhColor.find(ph.toInt())))
                    }

                    PhAnalyzerHelper.addAcidEventListener(object : AcidEventListener
                    {
                        override fun onAdd()
                        {
                            val statusColorResId = ContextCompat.getColor(requireContext(), R.color.ph0)

                            updateAllPhView(resources.getText(R.string.addAcidTxt), statusColorResId, statusColorResId, statusColorResId, R.drawable.ic_baseline_fast_rewind_red)
                        }
                    })

                    PhAnalyzerHelper.addAlkalineEventListener(object : AlkalineEventListener
                    {
                        override fun onAdd()
                        {
                            val statusColorResId = ContextCompat.getColor(requireContext(), R.color.ph13)

                            updateAllPhView(resources.getText(R.string.addBaseTxt), statusColorResId, statusColorResId, statusColorResId, R.drawable.ic_baseline_fast_forward_blue)
                        }
                    })

                    PhAnalyzerHelper.addNeutralEventListener(object : NeutralEventListener
                    {
                        override fun onStable()
                        {
                            updateAllPhView(
                                    "",
                                    0,
                                    phTxtVw.currentTextColor,
                                    ContextCompat.getColor(requireContext(), PhColor.find(phTxtVw.text.toString().toDouble().toInt())),
                                    null)
                        }
                    })

                    val threshold = data[HydrophonicApplication.THRESHOLD] as Double

                    val userNeutral = data[HydrophonicApplication.USER_NEUTRAL] as Double

                    PhAnalyzerHelper.analyze(Ph(ph, userNeutral + threshold, userNeutral - threshold, userNeutral))

                    //endregion
                }

                override fun onCancelled(error: DatabaseError)
                {
                    Toast.makeText(requireContext(), "Failed to get data", Toast.LENGTH_LONG).show()
                }
            }

            Firebase.database.reference.addValueEventListener(valueEventListener)
        }
    }

    /*
        The way this works is that, if it's neutral, almost everything is painted with the color of the current ph. If it's too acid or alkaline,
        all notifiers are painted with opposing scale direction, indicating needs more of the other direction. For example, if it's acid it'll
        paint indicators with base color and vice versa.

        All tags are used for UI tests
         */
    private fun updateAllPhView(statusDescTxt: CharSequence, statusDescTxtColorResId: Int, phTitleTxtColorResId: Int, statusDescIconColorResId: Int, @DrawableRes phStatusIconDrawableResId: Int?)
    {
        statusDescTxtVw.apply {
            text = statusDescTxt

            setTextColor(statusDescTxtColorResId)
        }

        phTitleTxtVw.setTextColor(phTitleTxtColorResId)

        statusDescIconImgVw.apply {
            tag = statusDescIconColorResId.toString()

            setColorFilter(statusDescIconColorResId)
        }

        //Means it's neutral
        if(phStatusIconDrawableResId == null)
        {
            phStatusIconImgVw.apply {
                visibility = View.INVISIBLE

                tag = ContextCompat.getColor(requireContext(), R.color.transparent).toString()
            }

            statusDescIconImgVw.apply {
                setImageResource(R.drawable.ic_round_check_circle_24)

                tag = (statusDescIconImgVw.tag as String) + "," + R.drawable.ic_round_check_circle_24.toString()
            }
        }

        else
        {
            statusDescIconImgVw.apply {
                tag = (statusDescIconImgVw.tag as String) + "," + R.drawable.ic_round_error_blue.toString()

                setImageResource(R.drawable.ic_round_error_blue)
            }

            phStatusIconImgVw.apply {
                visibility = View.VISIBLE

                tag = phStatusIconDrawableResId.toString()

                setImageResource(phStatusIconDrawableResId)
            }
        }
    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            NotificationChannel(
                    resources.getString(R.string.phNotificationChannelID),
                    resources.getString(R.string.phNotificationChannelName),
                    NotificationManager.IMPORTANCE_HIGH
            ).apply {
                description = resources.getString(R.string.phNotificationChannelDesc)

                requireActivity().getSystemService(NotificationManager::class.java).createNotificationChannel(this)
            }
        }

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        Toast.makeText(requireContext(), requireContext().packageManager.getPackageInfo(requireContext().packageName, 0).versionName, Toast.LENGTH_LONG).show()

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        inflater.inflate(R.menu.toolbar_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun updatePiStatusUI(statusTxt : String, statusColorId : Int, piStatusTextView: TextView)
    {
        piStatusTextView.text = statusTxt

        piStatusTextView.setTextColor(statusColorId)
    }

    override fun onConfigurationChanged(newConfig: Configuration)
    {
        super.onConfigurationChanged(newConfig)

        /*
        Remove it because Firebase.database.reference is a singleton and on config change will call create again and if not removed, it will add another instance of the same listener.
        This was causing problems accessing the context when a change occurred.
         */
        Firebase.database.reference.removeEventListener(valueEventListener)

        //Force a recreation of the current activity
        requireActivity().recreate()
    }
}