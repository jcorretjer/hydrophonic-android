package com.retroroots.hydrophonic.fragment

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.retroroots.hydrophonic.R
import com.retroroots.hydrophonic.ui.HydrophonicApplication
import com.retroroots.hydrophonic.ui.MainActivity
import com.retroroots.hydrophonic.util.phanalyzer.Ph
import com.retroroots.hydrophonic.util.phanalyzer.PhAnalyzerHelper
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AcidEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AlkalineEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.NeutralEventListener
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.not
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Test
import java.text.DecimalFormat

/*
The way to test is by changing this value 'ph = data[DBKeys.PH] as Double' to whatever double value. For this to work, the same has to be done for the ph value of the PhFragment.
 */

class PhFragmentTest
{

    private lateinit var activity : MainActivity

    private var threshold = 0.0

    private var userNeutral = 0.0

    private var ph = 0.0

    private var lastUpdated = ""

    @Before
    fun init()
    {
        launchActivity<MainActivity>().onActivity {
            activity = it
        }

        Thread.sleep(1000)
    }

    @Test
    fun uIMatchesPhTest()
    {
        Firebase.database.reference.addValueEventListener(object : ValueEventListener
       {
           override fun onDataChange(snapshot: DataSnapshot)
           {
               val data = snapshot.value!! as HashMap<*, *>

               lastUpdated = data[HydrophonicApplication.FirebaseDBKey.LAST_UPDATED] as String

               ph = data[HydrophonicApplication.FirebaseDBKey.PH] as Double

               threshold = data[HydrophonicApplication.FirebaseDBKey.THRESHOLD] as Double

               userNeutral = data[HydrophonicApplication.FirebaseDBKey.USER_NEUTRAL] as Double
           }

           override fun onCancelled(error: DatabaseError)
           {

           }
       })

        Thread.sleep(1000)

        onView(withId(R.id.circularProgressVwStub)).check(doesNotExist())

        onView(withId(R.id.phTxtVw)).check(matches(withText(DecimalFormat("00.00").format(ph))))

        PhAnalyzerHelper.addNeutralEventListener(object : NeutralEventListener
        {
            override fun onStable()
            {
                onView(withId(R.id.statusDescTxtVw)).check(matches(withText("")))

                val color = activity.findViewById<TextView>(R.id.phTxtVw).currentTextColor

                onView(withId(R.id.phTitleTxtVw)).check(matches(withTextViewTextColor(color)))

                onView(withId(R.id.phStatusIconImgVw)).check(matches(not(isDisplayed())))

                onView(withId(R.id.statusDescIconImgVw)).check(matches(withImageViewTag(R.drawable.ic_round_check_circle_24.toString())))
            }
        })

        PhAnalyzerHelper.addAlkalineEventListener(object : AlkalineEventListener
        {
            override fun onAdd()
            {
                val statusColorResId = ContextCompat.getColor(activity, R.color.ph13)

                R.id.statusDescTxtVw.apply {
                    onView(withId(this)).check(matches(withText(activity.resources.getText(R.string.addBaseTxt).toString())))

                    onView(withId(this)).check(matches(withTextViewTextColor(statusColorResId)))
                }

                R.id.statusDescTxtVw.apply {
                    onView(withId(this)).check(matches(withText(activity.resources.getText(R.string.addBaseTxt).toString())))

                    onView(withId(this)).check(matches(withTextViewTextColor(statusColorResId)))
                }

                onView(withId(R.id.phTitleTxtVw)).check(matches(withTextViewTextColor(statusColorResId)))

                R.id.statusDescIconImgVw.apply {
                    onView(withId(this)).check(matches(withImageViewTag(statusColorResId.toString())))

                    onView(withId(this)).check(matches(withImageViewTag(R.drawable.ic_round_error_blue.toString())))
                }

                onView(withId(R.id.phStatusIconImgVw)).check(matches(isDisplayed()))
            }
        })

        PhAnalyzerHelper.addAcidEventListener(object : AcidEventListener
        {
            override fun onAdd()
            {
                val statusColorResId = ContextCompat.getColor(activity, R.color.ph0)

                R.id.statusDescTxtVw.apply {
                    onView(withId(this)).check(matches(withText(activity.resources.getText(R.string.addAcidTxt).toString())))

                    onView(withId(this)).check(matches(withTextViewTextColor(statusColorResId)))
                }

                onView(withId(R.id.phTitleTxtVw)).check(matches(withTextViewTextColor(statusColorResId)))

                R.id.statusDescIconImgVw.apply {
                    onView(withId(this)).check(matches(withImageViewTag(statusColorResId.toString())))

                    onView(withId(R.id.statusDescIconImgVw)).check(matches(withImageViewTag(R.drawable.ic_round_error_blue.toString())))
                }

                onView(withId(R.id.phStatusIconImgVw)).check(matches(isDisplayed()))
            }
        })

        PhAnalyzerHelper.analyze(Ph(ph, userNeutral + threshold, userNeutral - threshold, userNeutral))

        println("Test over")
    }

    private fun withTextViewTextColor(colorId: Int) : Matcher<View>
    {
        return object : TypeSafeMatcher<View>()
        {
            override fun describeTo(description: Description?)
            {
                description?.appendText("TextView text should have $colorId as the color id.")
            }

            override fun matchesSafely(item: View?): Boolean
            {
                return (item as TextView).currentTextColor == colorId
            }
        }
    }

    private fun withImageViewTag(tag: String) : Matcher<View>
    {
        return object : TypeSafeMatcher<View>()
        {
            override fun describeTo(description: Description?)
            {
                description?.appendText("ImageView tag must contain $tag")
            }

            override fun matchesSafely(item: View?): Boolean
            {
                return ((item as ImageView).tag as String).contains(tag)
            }
        }
    }
}