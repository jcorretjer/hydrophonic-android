package com.retroroots.hydrophonic.util.pistatuscolorfinder

import com.retroroots.hydrophonic.R
import org.junit.Assert.assertEquals
import org.junit.Test

class PiStatusColorFinderTest
{
    @Test
    fun findRunningColorTest()
    {
        assertEquals(R.color.ph9, PiStatusColor.find(PiStatusColorFinder.PiStatus.RUNNING))
    }

    @Test
    fun findFailedColorTest()
    {
        assertEquals(R.color.ph2, PiStatusColor.find(PiStatusColorFinder.PiStatus.FAILED))
    }
}