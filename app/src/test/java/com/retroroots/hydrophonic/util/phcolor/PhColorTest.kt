package com.retroroots.hydrophonic.util.phcolor


import com.retroroots.hydrophonic.R
import com.retroroots.hydrophonic.util.phcolorfinder.PhColor
import org.junit.Assert.assertEquals
import org.junit.Test

class PhColorTest
{
    @Test
    fun findRedColorTest()
    {
        assertEquals(R.color.ph0, PhColor.find(0))
    }

    @Test
    fun findDefaultColorFromNonExistingScaleValueTest()
    {
        assertEquals(R.color.ph13, PhColor.find(-1))
    }
}