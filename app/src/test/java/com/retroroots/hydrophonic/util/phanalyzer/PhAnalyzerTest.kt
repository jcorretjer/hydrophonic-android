package com.retroroots.hydrophonic.util.phanalyzer

import com.retroroots.hydrophonic.util.phanalyzer.listeners.AcidEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.AlkalineEventListener
import com.retroroots.hydrophonic.util.phanalyzer.listeners.NeutralEventListener
import org.junit.Assert.assertTrue
import org.junit.Test

class PhAnalyzerTest
{
    private var passed = false

    private  companion object
    {
        const val USER_NEUTRAL = 7.0

        const val THRESHOLD = 0.5

        const val ACID_THRESHOLD = USER_NEUTRAL - THRESHOLD

        const val BASE_THRESHOLD = USER_NEUTRAL + THRESHOLD
    }

    @Test
    fun onHighAcidTest()
    {
        PhAnalyzerHelper.addAlkalineEventListener(object : AlkalineEventListener
        {
            override fun onAdd()
            {
                passed = true
            }
        })

        PhAnalyzerHelper.analyze(Ph(1.0, BASE_THRESHOLD, ACID_THRESHOLD, USER_NEUTRAL))

        assertTrue(passed)
    }

    @Test
    fun onHighBaseTest()
    {
        PhAnalyzerHelper.addAcidEventListener(object : AcidEventListener
        {
            override fun onAdd()
            {
                passed = true
            }
        })

        PhAnalyzerHelper.analyze(Ph(10.0, BASE_THRESHOLD, ACID_THRESHOLD, USER_NEUTRAL))

        assertTrue(passed)
    }

    @Test
    fun onNeutralTest()
    {
        PhAnalyzerHelper.addNeutralEventListener(object : NeutralEventListener
        {
            override fun onStable()
            {
                passed = true
            }
        })

        PhAnalyzerHelper.analyze(Ph(7.0, BASE_THRESHOLD, ACID_THRESHOLD, USER_NEUTRAL))

        assertTrue(passed)
    }

    @Test
    fun onEmptyListenersTest()
    {
        PhAnalyzerHelper.analyze(Ph(7.0, BASE_THRESHOLD, ACID_THRESHOLD, USER_NEUTRAL))

        assertTrue(!passed)
    }
}