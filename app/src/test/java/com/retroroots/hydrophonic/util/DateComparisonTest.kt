package com.retroroots.hydrophonic.util

import com.retroroots.hydrophonic.util.datecomparison.DateComparisonHelper
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class DateComparisonTest
{
    private lateinit var lesserDate : Date

    private lateinit var greaterDate : Date

    @Test
    fun greaterDateGreaterThanLessDateBy10MinutesTest()
    {
        lesserDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 11:00:00") !!

        greaterDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 11:11:00") !!

        assertTrue(DateComparisonHelper.greaterThan(lesserDate, greaterDate, Calendar.MINUTE, 10))
    }

    @Test
    fun greaterDateGreaterThanD2By10MinutesWhereD2IsGreaterByDaysTest()
    {
        lesserDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 11:00:00") !!

        greaterDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/10/2000 11:00:00") !!

        assertTrue(DateComparisonHelper.greaterThan(lesserDate, greaterDate, Calendar.MINUTE, 10))
    }

    @Test
    fun greaterDateSameAsLessDateByTest()
    {
        lesserDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 11:00:00") !!

        greaterDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 11:00:00") !!

        assertFalse(DateComparisonHelper.greaterThan(lesserDate, greaterDate, Calendar.MINUTE, 0))
    }

    @Test
    fun greaterDateGreaterThanLessDateBy1HourTest()
    {
        lesserDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 09:00:00") !!

        greaterDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 10:00:00") !!

        assertTrue(DateComparisonHelper.greaterThan(lesserDate, greaterDate, Calendar.HOUR, 1))
    }

    @Test
    fun greaterDateLesserThanLessDateBy1HourTest()
    {
        lesserDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 10:00:00") !!

        greaterDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault()).parse("01/01/2000 09:00:00") !!

        assertFalse(DateComparisonHelper.greaterThan(lesserDate, greaterDate, Calendar.HOUR, 1))
    }
}